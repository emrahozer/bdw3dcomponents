﻿using System;

namespace App.Scripts.Integration.EventArguments
{
    public class OpenPlotEventArgs : EventArgs
    {
        public string PlotId;
        public OpenPlotEventArgs(string plotId)
        {
            PlotId = plotId;
        }
    }
}
﻿using System;

namespace App.Scripts.Integration.EventArguments
{
    public class CameraModeEventArguments : EventArgs
    {
        public bool IsInFpsMode;

        public CameraModeEventArguments(bool isInFpsMode)
        {
            IsInFpsMode = isInFpsMode;
        }
    }
}

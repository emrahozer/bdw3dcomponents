﻿using App.Scripts.Model;
using TMPro;
using UnityEngine;

namespace App.Scripts.Markers
{
    [ExecuteInEditMode]
    public class MarkerInteractiveRegion : InteractiveRegion
    {
        public Material AvailableMaterial;
        public Material ReservedMaterial;
        public Material SoldMaterial;
        public MeshRenderer QuadRenderer;
        public TMP_Text PlotNumberText;

        void Awake()
        {
            MeshRenderer = QuadRenderer;
            SetMaterials();
        }

        public void SetMaterials()
        {
            InternalAvailableMaterial = AvailableMaterial;
            InternalReservedMaterial = ReservedMaterial;
            InternalSoldMaterial = SoldMaterial;
        }

        void Update()
        {
            if (Application.isPlaying && AppLogic.Instance.CurrentCamera != null)
                transform.LookAt(transform.position + AppLogic.Instance.CurrentCamera.transform.rotation * Vector3.forward, AppLogic.Instance.CurrentCamera.transform.rotation * Vector3.up);

            PlotNumberText.text = PlotId.ToString();
        }
    }
}

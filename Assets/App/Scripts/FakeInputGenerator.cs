﻿using System.Collections;
using System.Collections.Generic;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using App.Scripts.Model;
using UnityEngine;

namespace App.Scripts
{
    public class FakeInputGenerator : MonoBehaviour
    {
        private ManipulationEventArgs args;
        private bool _isDirty = false;

        // Use this for initialization
        IEnumerator Start()
        {
            args = new ManipulationEventArgs(0, 0, 0, 1,false);


#if UNITY_EDITOR
            IT_Gesture.onDraggingE += ItGestureOnDraggingE;
            IT_Gesture.onPinchE += ItGestureOnPinch;
            IT_Gesture.onRotateE += ItGestureOnRotate;
            IT_Gesture.onShortTapE += OnShortTap;
            IT_Gesture.onDualFDraggingE += OnDoubleFingerDraggingE;
            UnityUwpInterop.Instance.IsInInteraction = true;
#endif
            yield return new WaitForSeconds(1);
            // Set a fake status
            
            var status = new Dictionary<string, int>() { { "1", 2 }, { "2", 2 }, { "3", 1 }, { "4", 2 }, { "5", 1 }, { "6", 1 }, { "7", 1 }, { "62", 3 } };
            UnityUwpInterop.Instance.StatusDictionary = status;
            UnityUwpInterop.Instance.RaiseStatusUpdate();

            // Set a fake filter data
            var filterStatus = new Dictionary<string, bool> { { "1", false }, { "2", false }, { "3", true }, { "4", false }, { "5", true}, { "6", false}, { "7", false}, { "8", true } };
            UnityUwpInterop.Instance.IsInFilterDictionary = filterStatus;
            UnityUwpInterop.Instance.RaiseFilterUpdate();

            // Set a fake filter data
            yield return new WaitForSeconds(2);
            var filterStatusEnable = new Dictionary<string, bool> { { "1", true }, { "2", true}, { "3", true }, { "4", true }, { "5", true }, { "6", false }, { "7", false }, { "8", true } };
            UnityUwpInterop.Instance.IsInFilterDictionary = filterStatusEnable;
            UnityUwpInterop.Instance.RaiseFilterUpdate();
        }


        void OnDisable()
        {
#if UNITY_EDITOR
            IT_Gesture.onDraggingE -= ItGestureOnDraggingE;
            IT_Gesture.onPinchE -= ItGestureOnPinch;
            IT_Gesture.onRotateE -= ItGestureOnRotate;
            IT_Gesture.onShortTapE -= OnShortTap;
            IT_Gesture.onDualFDraggingE -= OnDoubleFingerDraggingE;

#endif
        }

        private void OnShortTap(Vector2 pos)
        {
            UnityUwpInterop.Instance.ReceiveTap(pos.x,pos.y);
        }

        private void ItGestureOnRotate(RotateInfo ri)
        {
            args.RotationDelta = ri.magnitude;
            _isDirty = true;
        }

        private void ItGestureOnPinch(PinchInfo pi)
        {
            args.ScaleDelta = 1 - (1 / pi.magnitude);
            _isDirty = true;
        }

        private void ItGestureOnDraggingE(DragInfo dragInfo)
        {
            args.TranslationDelta = dragInfo.delta;
            _isDirty = true;
        }

        private void OnDoubleFingerDraggingE(DragInfo draginfo)
        {
            args.MultiFinger = true;
            args.TranslationDelta = draginfo.delta;
            UnityUwpInterop.Instance.ReceiveManipulation(args.TranslationDelta.x, args.TranslationDelta.y, args.RotationDelta, args.ScaleDelta, args.MultiFinger);
        }

        private void Update()
        {
            if (_isDirty)
            {
                UnityUwpInterop.Instance.ReceiveManipulation(args.TranslationDelta.x, args.TranslationDelta.y, args.RotationDelta, args.ScaleDelta, args.MultiFinger);
                args = new ManipulationEventArgs(0, 0, 0, 1,false);
                _isDirty = false; 
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (UnityUwpInterop.Instance.IsInStreetMode)
                    UnityUwpInterop.Instance.IsInStreetMode = false;
            }
        }
    }
}
﻿using App.Scripts;
using UnityEngine;

public class GameComponents : MonoBehaviour {

    public Transform ModelContainer;
    public Transform InteractivityContainer;
    public Transform FpsInteractiveRegions;
    public Transform RtsInteractiveRegions;

    public Material AvailableMeshMaterial;
    public Material ReservedMeshMaterial;
    public Material SoldMeshMaterial;

    public Camera RtsCamera;
    public Camera FpsCamera;

    private void Start()
    {
        var appLogic = this.gameObject.AddComponent<AppLogic>();
        appLogic.Components = this;
        appLogic.Initialize();
       
    }
}

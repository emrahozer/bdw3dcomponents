﻿using System.Collections;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using UnityEngine;

namespace App.Scripts.CameraControllers
{
    public class FpsCameraController : MonoBehaviour
    {
     
        public Vector3 DesiredPosition
        {
            get { return _desiredPosition; }
            set { _desiredPosition = value; }
        }

        private Camera _camera;
        private Vector3 _cameraVelocity;
        private float _smoothness = 0.4f;
        private Vector3 _desiredPosition = Vector3.zero;

        private FpsCameraControllerProperties _properties;

        public void Start()
        {
            _properties = GetComponent<FpsCameraControllerProperties>();
            _camera = GetComponent<Camera>();
            var transformPosition = _camera.transform.position;
            _camera.transform.position = new Vector3(transformPosition.x, _properties.CameraHeight, transformPosition.z);
        }

        private void OnEnable()
        {
            UnityUwpInterop.Instance.ManipulationReceived += OnManipulationReceived;
        }

        private void OnDisable()
        {
            UnityUwpInterop.Instance.ManipulationReceived -= OnManipulationReceived;
        }

        private void OnManipulationReceived(object sender, ManipulationEventArgs e)
        {
            UnityMainThreadDispatcher.Enqueue(ApplyManipulation(e));
        }

        public void Update()
        {
            if (_desiredPosition != Vector3.zero)
                _camera.transform.position = Vector3.SmoothDamp(_camera.transform.position, DesiredPosition, ref _cameraVelocity, _smoothness);
        }

        private IEnumerator ApplyManipulation(ManipulationEventArgs e)
        {
            if (transform != null)
            {
                // vertical
                var rotationX = transform.localEulerAngles.x + e.TranslationDelta.y * _properties.SpeedCoefficient;
                // horizontal rotation
                var rotationY = transform.localEulerAngles.y + e.TranslationDelta.x * _properties.SpeedCoefficient;

                transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
            }

            yield return null;
        }
    }
}

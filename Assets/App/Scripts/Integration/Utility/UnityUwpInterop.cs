﻿using System;
using System.Collections.Generic;
using App.Scripts.Integration.EventArguments;

namespace App.Scripts.Integration.Utility
{
    public class UnityUwpInterop
    {
        #region Singleton

        private static UnityUwpInterop _instance;

        private UnityUwpInterop()
        {
            IsInFilterDictionary = new Dictionary<string, bool>();
            StatusDictionary = new Dictionary<string, int>();
        }

        public static UnityUwpInterop Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UnityUwpInterop();
                }

                return _instance;
            }
        }

        #endregion

        public event EventHandler<OpenPlotEventArgs> PlotOpen;
        public event EventHandler<ManipulationEventArgs> ManipulationReceived;
        public event EventHandler<TapEventArguments> TapReceived;
        public event EventHandler<LoadSiteplanEventArgs> LoadSiteplan;
        public event EventHandler SceneLoaded;
        public event EventHandler InteractionStatusChanged;
        public event EventHandler StatusUpdated;
        public event EventHandler FilterUpdated;
        public event EventHandler AppReset;
        public event EventHandler<CameraModeEventArguments> StreetModeUpdated;
        public bool IsSceneLoaded;
        public Dictionary<string, bool> IsInFilterDictionary;
        public Dictionary<string, int> StatusDictionary;

        public void DispatchOpenPlotEvent(string regionPlotId)
        {
            if (PlotOpen != null)
                PlotOpen.Invoke(this, new OpenPlotEventArgs(regionPlotId));
        }

        private bool _isInInteraction = false;

        public bool IsInInteraction
        {
            get { return _isInInteraction; }
            set
            {
                _isInInteraction = value;
                if (InteractionStatusChanged != null) InteractionStatusChanged.Invoke(this, EventArgs.Empty);
            }
        }

        public bool _isInStreetMode;
        public bool IsInStreetMode
        {
            get { return _isInStreetMode; }
            set
            {
                _isInStreetMode = value;
                if (StreetModeUpdated != null) StreetModeUpdated(this, new CameraModeEventArguments(_isInStreetMode));
            }
        }

        public void ReceiveManipulation(float translationX, float translationY, float rotationDelta, float scaleDelta, bool multiFinger )
        {
            if (ManipulationReceived != null)
                ManipulationReceived.Invoke(this, new ManipulationEventArgs(translationX, translationY, rotationDelta, scaleDelta, multiFinger));
        }

        public void ReceiveTap(float x, float y)
        {
            if (TapReceived != null)
                TapReceived.Invoke(this, new TapEventArguments(x, y));
        }

        public void LoadAssetFor3DSiteplan(string path, string sceneName)
        {
            if (LoadSiteplan != null)
                LoadSiteplan.Invoke(this, new LoadSiteplanEventArgs(path, sceneName));
        }

        public void RaiseStatusUpdate()
        {
            if(StatusUpdated != null)
                StatusUpdated.Invoke(this, EventArgs.Empty);
        }

        public void RaiseFilterUpdate()
        {
            if(FilterUpdated != null)
                FilterUpdated.Invoke(this, EventArgs.Empty);
        }

        public void RaiseSceneLoaded()
        {
            if(SceneLoaded != null)
                SceneLoaded.Invoke(this, EventArgs.Empty);
            IsSceneLoaded = true;
        }

        public void RaiseAppReset()
        {
            if(AppReset != null)
                AppReset.Invoke(this, EventArgs.Empty);
        }

    }
}
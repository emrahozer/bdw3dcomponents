﻿using System;
using System.Collections;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using UnityEngine;

namespace App.Scripts.Integration
{
    public class LoadScenes : MonoBehaviour
    {
        private string _path;
        private string _sceneName;
        public string SceneAssetBundle;
        public string SceneName;

        private IEnumerator Start()
        {
            Debug.Log("start");
            UnityUwpInterop.Instance.LoadSiteplan += OnLoadSiteplan;

#if UNITY_EDITOR
            Debug.Log("scene is loading");
            _sceneName = SceneName;
            yield return StartCoroutine(LoadAndInitialize());
#else
        yield return null;
#endif
        }

        protected IEnumerator Initialize()
        {
#if UNITY_EDITOR
            AssetBundleManager.AssetBundleManager.SetDevelopmentAssetBundleServer();
#endif
            var request = AssetBundleManager.AssetBundleManager.Initialize();
            if (request != null)
                yield return StartCoroutine(request);
        }

        private void OnLoadSiteplan(object sender, LoadSiteplanEventArgs e)
        {
            _path = e.Path;
            _sceneName = e.SceneName;
            UnityMainThreadDispatcher.Enqueue(LoadAndInitialize());
        }

        private IEnumerator LoadAndInitialize()
        {
            yield return StartCoroutine(Load());
            yield return StartCoroutine(InitializeLevelAsync(_sceneName, false));
        }

        private IEnumerator Load()
        {
            AssetBundleManager.AssetBundleManager.SetSourceAssetBundleURL(_path);

            var request = AssetBundleManager.AssetBundleManager.Initialize();
            if (request != null)
                yield return StartCoroutine(request);
        }
        protected IEnumerator InitializeLevelAsync(string levelName, bool isAdditive)
        {
            var request = AssetBundleManager.AssetBundleManager.LoadLevelAsync(SceneAssetBundle, levelName, isAdditive);
            if (request == null)
                yield break;
            yield return StartCoroutine(request);

        }

    }
}
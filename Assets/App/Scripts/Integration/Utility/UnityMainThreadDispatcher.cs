﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Scripts.Integration.Utility
{
    public class UnityMainThreadDispatcher : MonoBehaviour {

        private static readonly Queue<IEnumerator> _executionQueue = new Queue<IEnumerator>();
        private static UnityMainThreadDispatcher _instance;

        public static void Enqueue(IEnumerator action) {
            lock (_executionQueue) {
                _executionQueue.Enqueue(action);
            }
        }
	
        public static void Enqueue(Action action) {
            Enqueue(_instance.ActionWrapper(action));
        }

        private void Awake() {
            if (_instance != null) {
                Destroy(gameObject);
            }
            else {
                _instance = this;
            }
        }

        private void Start()
        {
            DontDestroyOnLoad(this);
        }

        private void Update() {
            lock(_executionQueue) {
                while (_executionQueue.Count > 0) {
                    StartCoroutine(_executionQueue.Dequeue());
                }
            }
        }

        IEnumerator ActionWrapper(Action a) {
            a();
            yield return null;
        }
    }
}


﻿using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using UnityEngine;

namespace App.Scripts.CameraControllers
{
    public class RtsCameraController : MonoBehaviour
    {
        private RtsCameraControllerProperties _properties;
        private float _distance;
        private Vector3 _cameraVelocity;
        private float _rotation = 0.0f;
        private float _tilt = 0.66f;
        private Vector3 _cameraTargetPosition;
        private Camera _camera;

        private void Start()
        {
            _properties = GetComponent<RtsCameraControllerProperties>();
            _camera = gameObject.GetComponent<Camera>();
            _cameraVelocity = Vector3.zero;
            _distance = (_properties.MaxDistance - _properties.MinDistance) / 2;
            _tilt = (_properties.MaxTilt - _properties.MinTilt) / 2;
            _cameraTargetPosition = Vector3.zero;
            SetCameraToInitialValues();
        }

        private void SetCameraToInitialValues()
        {
            float posX = _distance * Mathf.Cos(_rotation) - _cameraTargetPosition.x;
            float posY = _distance * Mathf.Sin(_tilt);
            float posZ = _distance * Mathf.Sin(_rotation) - _cameraTargetPosition.z;

            _camera.transform.position = new Vector3(posX + 2.0f, posY, posZ);
            _camera.transform.LookAt(_cameraTargetPosition);
        }

        private void OnEnable()
        {
            UnityUwpInterop.Instance.ManipulationReceived += OnManipulation;
        }

        private void OnDisable()
        {
            UnityUwpInterop.Instance.ManipulationReceived -= OnManipulation;
        }

        private void OnManipulation(object sender, ManipulationEventArgs e)
        {
            float distanceEffectOnCoef = 0.5f + (0.5f * ((_distance - _properties.MinDistance) / (_properties.MaxDistance - _properties.MinDistance)));

            if (!e.MultiFinger)
            {
                _rotation += e.TranslationDelta.x * (_properties.TransitionMultiplier * distanceEffectOnCoef);
                _tilt += e.TranslationDelta.y * (_properties.TransitionMultiplier * distanceEffectOnCoef);
                _distance /= e.ScaleDelta;
            }
            else
            {
                UnityMainThreadDispatcher.Enqueue(() =>
                {
                    var deltaX = e.TranslationDelta.x * distanceEffectOnCoef * _properties.CameraTargetManipulationMultipliers * (_properties.CameraTargetInverseX ? -1 : 1);
                    var deltaY = e.TranslationDelta.y * distanceEffectOnCoef * _properties.CameraTargetManipulationMultipliers * (_properties.CameraTargetInverseY ? -1 : 1);
                    var newPos = Quaternion.Euler(0, _camera.transform.rotation.eulerAngles.y, 0) * new Vector3(deltaX, 0, deltaY);
                    _cameraTargetPosition = _cameraTargetPosition + newPos;
                });
            }
        }

        private void LateUpdate()
        {
            _rotation = Mod(_rotation, Mathf.PI * 2.0f);
            _tilt = Mathf.Clamp(_tilt, _properties.MinTilt, _properties.MaxTilt);
            _distance = Mathf.Clamp(_distance, _properties.MinDistance, _properties.MaxDistance);

            float posX = _cameraTargetPosition.x + _distance * Mathf.Cos(_rotation);
            float posY = _distance * Mathf.Sin(_tilt);
            float posZ = _cameraTargetPosition.z + _distance * Mathf.Sin(_rotation);

            _camera.transform.position = Vector3.SmoothDamp(_camera.transform.position, new Vector3(posX, posY, posZ), ref _cameraVelocity, _properties.Smoothness);
            _camera.transform.LookAt(_cameraTargetPosition);
        }

        private float Mod(float x, float m)
        {
            float r = x % m;
            return r < 0.0f ? r + m : r;
        }

        public void Reset()
        {
            _distance = (_properties.MaxDistance - _properties.MinDistance) / 2;
            _tilt = (_properties.MaxTilt - _properties.MinTilt) / 2;
            _rotation = 0.0f;
        }
    }
}

﻿using UnityEngine;

namespace App.Scripts.Markers
{
    public class VrHotSpot : MonoBehaviour
    {
        private void Update()
        {
            if (Application.isPlaying && AppLogic.Instance!=null && AppLogic.Instance.CurrentCamera != null)
            {
                var currentCamera = AppLogic.Instance.CurrentCamera;
                if (currentCamera)
                {
                    transform.LookAt(transform.position + currentCamera.transform.rotation * Vector3.forward, currentCamera.transform.rotation * Vector3.up);
                }
                else
                {
                    Debug.Log("current camera is null");
                }
            }
        }
    }
}

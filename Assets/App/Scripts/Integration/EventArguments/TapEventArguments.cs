﻿using System;
using UnityEngine;

namespace App.Scripts.Integration.EventArguments
{
    public class TapEventArguments : EventArgs
    {
        public Vector2 Position;
        public TapEventArguments(float x, float y)
        {
            Position = new Vector2(x, y);
        }
    }
}

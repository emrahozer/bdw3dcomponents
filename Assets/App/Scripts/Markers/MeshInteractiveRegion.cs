﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace App.Scripts.Markers
{
    [RequireComponent(typeof(MeshRenderer))]
    public class MeshInteractiveRegion : InteractiveRegion
    {
        private void Awake()
        {      
            MeshRenderer = GetComponent<MeshRenderer>();
        }

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            MeshRenderer = this.GetComponent<MeshRenderer>();
            if (PlotId != -1 && MeshRenderer != null)
            {
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.black;
                Handles.Label(MeshRenderer.bounds.center, PlotId.ToString(), style);
            }
        }
#endif
    }
}
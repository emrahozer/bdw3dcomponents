﻿using UnityEngine;

namespace App.Scripts.CameraControllers
{
    public class RtsCameraControllerProperties : MonoBehaviour
    {
        public float MinDistance = 100.0f;
        public float MaxDistance = 300.0f;
        public float Smoothness = 0.3f;
        public float ScaleSmoothness = 0.96f;
        public float TransitionMultiplier = 0.01f;
        public float CameraTargetManipulationMultipliers = 0.05f;
        public bool CameraTargetInverseX= false;
        public bool CameraTargetInverseY = false;

        public float MinTilt = 0.2f;
        public float MaxTilt = 0.8f;
      
    }
}
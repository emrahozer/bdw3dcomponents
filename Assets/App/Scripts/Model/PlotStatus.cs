﻿namespace App.Scripts.Model
{
    public enum PlotStatus
    {
        Available = 1,
        Reserved = 2,
        Sold = 3,
        Unreleased = 4,
        Unlisted = 5
    }
}
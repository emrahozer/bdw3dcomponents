﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteRotate : MonoBehaviour
{
    public float Speed;
    private RectTransform _rectTransform;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    void Update () {
	    _rectTransform.Rotate(new Vector3(0, 0, Speed * Time.deltaTime));
	}
}

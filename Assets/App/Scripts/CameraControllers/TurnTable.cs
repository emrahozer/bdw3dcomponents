﻿using System.Collections;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using UnityEngine;

namespace App.Scripts.CameraControllers
{
    public class TurnTable : MonoBehaviour
    {
        [Range(0.1f, 10.0f)]
        public float RotateSpeedModifier = 2.0f;

        [Range(0.0f, 0.9f)]
        public float RotateInertiaCoefficient = 0.95f;

        public bool AllowRotate = true;

        private float _rotateSpeed;
        void Start()
        {
        
             UnityUwpInterop.Instance.ManipulationReceived += OnManipulationReceived;
        }

        private void OnManipulationReceived(object sender, ManipulationEventArgs e)
        {
            _rotateSpeed += e.RotationDelta * RotateSpeedModifier;
        }

        public IEnumerator RotateTurnTable(float delta)
        {
            transform.Rotate(Vector3.up, delta);
            yield return null;
        }

        private void Update()
        {
            if (AllowRotate)
            {
                transform.Rotate(Vector3.up, _rotateSpeed * Time.deltaTime);
                _rotateSpeed *= RotateInertiaCoefficient;
            }
        }
    }
}
﻿using System;
using UnityEngine;

namespace App.Scripts.Integration.EventArguments
{
    public class ManipulationEventArgs : EventArgs
    {
        public Vector2 TranslationDelta;
        public float RotationDelta;
        public float ScaleDelta;
        public bool MultiFinger;
        public ManipulationEventArgs(float x, float y, float rotationDelta, float scaleDelta, bool multiFinger)
        {
            TranslationDelta = new Vector2(x, y);
            RotationDelta = rotationDelta;
            ScaleDelta = scaleDelta;
            MultiFinger = multiFinger;

        }
    }
}

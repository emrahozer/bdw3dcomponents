﻿using App.Scripts.Model;
using UnityEngine;

namespace App.Scripts.Markers
{
    public class InteractiveRegion : MonoBehaviour
    {
        protected MeshRenderer MeshRenderer;
        private bool _isInFilter = true;
        private bool _isLoaded = false;
        private PlotStatus _status;

        public int PlotId;

        public Material InternalAvailableMaterial { get; set; }
        public Material InternalReservedMaterial { get; set; }
        public Material InternalSoldMaterial { get; set; }

        public PlotStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                UpdateMaterialsAndVisibility();
            }
        }

        public bool IsInFilter
        {
            get { return _isInFilter; }
            set
            {
                _isInFilter = value; 
                UpdateMaterialsAndVisibility();
            }
        }

        public virtual void UpdateMaterialsAndVisibility()
        {
            if(!_isLoaded)
                return;

            gameObject.SetActive(_isInFilter);

            switch (Status)
            {
                case PlotStatus.Available:
                    MeshRenderer.material = InternalAvailableMaterial;
                    break;
                case PlotStatus.Reserved:
                    MeshRenderer.material = InternalReservedMaterial;
                    break;
                case PlotStatus.Sold:
                    MeshRenderer.material = InternalSoldMaterial;
                    break;
                case PlotStatus.Unlisted:
                case PlotStatus.Unreleased:
                    this.gameObject.SetActive(false);
                    break;
            }
        }

        public void Start()
        {
            _isLoaded = true;
            UpdateMaterialsAndVisibility();
        }
    }
}
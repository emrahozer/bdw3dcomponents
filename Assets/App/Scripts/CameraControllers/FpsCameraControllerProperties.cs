﻿using System.Collections;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using UnityEngine;

namespace App.Scripts.CameraControllers
{
    public class FpsCameraControllerProperties : MonoBehaviour
    {
        public float SpeedCoefficient = 0.1f;
        public float CameraHeight;
    }
}

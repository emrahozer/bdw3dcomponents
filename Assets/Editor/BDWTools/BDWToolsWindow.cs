﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using App.Scripts;
using App.Scripts.AssetBundleManager;
using App.Scripts.AssetBundleManager.Editor;
using App.Scripts.Markers;
using ICSharpCode.SharpZipLib.Zip;
using UnityEditor;
using UnityEngine;

namespace BDWTools
{
    public class BDWToolsWindow : EditorWindow
    {
        private enum BDWTabs
        {
            Setup = 0,
            Validate = 1,
            Publish = 2
        }

        public Rect WindowRect;
        private string ApiURL;
        private string DevelopmentID;
        private string DevelopmentRegistrationKey;
        private BDWTabs _selectedTab;
        private WWW _getPlotsService;
        private Vector2 _scrollPosition;
        private GetPlotsServiceDO _getPlotsServiceResult;

        private int _totalCompressedFileCount = 0;
        private int _numberOfFilesToBeCompressed;

        public BDWToolsWindow()
        {
            WindowRect = new Rect(0, 0, 500, 400);
        }

        [MenuItem("Tools/BDW Tools")]
        static void Init()
        {
            BDWToolsWindow window = (BDWToolsWindow) EditorWindow.GetWindow(typeof(BDWToolsWindow));
            window.minSize = new Vector2(600, 400);
            window.Show();
        }

        private void OnEnable()
        {
            ApiURL = EditorPrefs.GetString("ApiURL");
            DevelopmentID = EditorPrefs.GetString("DevelopmentID");
            DevelopmentRegistrationKey = EditorPrefs.GetString("DevelopmentRegistrationKey");
            _getPlotsService = null;
        }

        void OnGUI()
        {
            GUILayout.BeginVertical();
            {
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Toggle(_selectedTab == BDWTabs.Setup, "SETUP", EditorStyles.toolbarButton))
                        _selectedTab = BDWTabs.Setup;

                    if (GUILayout.Toggle(_selectedTab == BDWTabs.Validate, "VALIDATE", EditorStyles.toolbarButton))
                        _selectedTab = BDWTabs.Validate;

                    if (GUILayout.Toggle(_selectedTab == BDWTabs.Publish, "PUBLISH", EditorStyles.toolbarButton))
                        _selectedTab = BDWTabs.Publish;
                }
                GUILayout.EndHorizontal();

                DrawContent(_selectedTab);
            }
            GUILayout.EndVertical();
        }

        private void DrawContent(BDWTabs tab)
        {
            switch (tab)
            {
                case BDWTabs.Setup:
                    DrawSetupTab();
                    break;
                case BDWTabs.Validate:
                    DrawValidateTab();
                    break;
                case BDWTabs.Publish:
                    DrawPublishTab();
                    break;
            }
        }

        private void DrawSetupTab()
        {
            GUILayout.Label("Development Settings", EditorStyles.boldLabel);
            ApiURL = EditorGUILayout.TextField("API URL", ApiURL);
            DevelopmentID = EditorGUILayout.TextField("Development ID", DevelopmentID);
            DevelopmentRegistrationKey = EditorGUILayout.TextField("Registration Key", DevelopmentRegistrationKey);
            var saveButton = GUILayout.Button(new GUIContent() {text = "SAVE"});
            if (saveButton)
                SaveSettings();
        }

        private void DrawValidateTab()
        {
            CheckIfDevelopmentIsSetCorrectly();
            var enableState = GUI.enabled;

            GUI.enabled = (_getPlotsService == null);
            var validateButton = GUILayout.Button(new GUIContent() {text = "VALIDATE"});
            if (validateButton)
            {
                CallGetPlotsService();
            }
            GUI.enabled = enableState;

            ValidateThatFakeInputManagerIsDeleted();

            if (_getPlotsServiceResult != null)
            {
                ValidatePlots();
            }
        }

        private void ValidateThatFakeInputManagerIsDeleted()
        {
            var fakeInputManager = FindObjectOfType<FakeInputGenerator>();
            if (fakeInputManager != null)
            {
                EditorGUILayout.HelpBox("Delete Fake Input Manager from the scene before publishing", MessageType.Error);
            }
        }

        private void ValidatePlots()
        {
            if (_getPlotsServiceResult.status == 0)
            {
                var listOfInteractiveRegions = FindObjectsOfType<InteractiveRegion>();
                var listOfMissingPlotNumbers = new List<int>();

                foreach (var plotDo in _getPlotsServiceResult.result)
                {
                    var plotNumber = int.Parse(plotDo.plotNumber);
                    if (listOfInteractiveRegions.All(region => region.PlotId != plotNumber))
                    {
                        listOfMissingPlotNumbers.Add(plotNumber);
                    }
                }

                _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.ExpandWidth(true), GUILayout.Height(400));
                foreach (int plotNumber in listOfMissingPlotNumbers)
                {
                    EditorGUILayout.HelpBox(string.Format("Can't find interactive region with number {0}", plotNumber), MessageType.Warning);
                }
                if (listOfMissingPlotNumbers.Count == 0)
                {
                    GUILayout.Label("All of the plots are assigned.");
                }
                GUILayout.EndScrollView();
            }
            else
            {
                EditorGUILayout.HelpBox(_getPlotsServiceResult.error, MessageType.Error, true);
            }
        }


        private void DrawPublishTab()
        {
            CheckIfDevelopmentIsSetCorrectly();
            var buildAndZip = GUILayout.Button("Build asset bundle and prepare for upload");
            string assetBundleRootFolder = Path.Combine(Environment.CurrentDirectory, Utility.AssetBundlesOutputPath);
            string outputPath = Path.Combine(assetBundleRootFolder, "Windows");

            EditorGUILayout.HelpBox("Please click the button and upload \"siteplan.zip\" file to the backend from the folder which will be opened after the process is completed", MessageType.Info);

            if (buildAndZip)
            {
                BuildScript.BuildAssetBundles();

                string filePath = Path.Combine(assetBundleRootFolder, "siteplan.zip");

                var file = ZipFile.Create(filePath);
                file.BeginUpdate();
                file.AddDirectory("Windows");
                _numberOfFilesToBeCompressed = Directory.GetFiles(outputPath, "*.*").Length;

                CompressDirectory(outputPath, file);
                file.CommitUpdate();
                file.Close();

                //open folder of zip
                Process.Start(assetBundleRootFolder);
                EditorUtility.ClearProgressBar();
            }
        }

        private void CallGetPlotsService()
        {
            var requestUriString = ApiURL + "GetPlots?developmentId=" + DevelopmentID +
                                   "&version=1" + "&ignoreLock=0";

            var headers = new Dictionary<string, string>();
            headers.Add("Registration-Key", DevelopmentRegistrationKey);
            _getPlotsService = new WWW(requestUriString, null, headers);
        }

        private void Update()
        {
            if (_getPlotsService != null && _getPlotsService.isDone)
            {
                var resultText = System.Text.Encoding.UTF8.GetString(_getPlotsService.bytes);
                _getPlotsServiceResult = JsonUtility.FromJson<GetPlotsServiceDO>(resultText);
                _getPlotsService = null;
            }
        }


        private void CheckIfDevelopmentIsSetCorrectly()
        {
            if (string.IsNullOrEmpty(DevelopmentRegistrationKey) || string.IsNullOrEmpty(DevelopmentID) || string.IsNullOrEmpty(ApiURL))
                EditorGUILayout.HelpBox("PLEASE COMPLETE THE SETUP", MessageType.Warning);
        }

        private void SaveSettings()
        {
            EditorPrefs.SetString("ApiURL", ApiURL);
            EditorPrefs.SetString("DevelopmentID", DevelopmentID);
            EditorPrefs.SetString("DevelopmentRegistrationKey", DevelopmentRegistrationKey);
        }

        private void OnDisable()
        {
            SaveSettings();
        }

        private void CompressDirectory(string directory, ZipFile zipFile)
        {
            var subDirectories = Directory.GetDirectories(directory);
            var directoryName = Path.GetDirectoryName(directory);
            var files = Directory.GetFiles(directory);

            foreach (var subDirectory in subDirectories)
            {
                var subDirectoryName = Path.GetDirectoryName(subDirectory);
                zipFile.AddDirectory(subDirectoryName);
                CompressDirectory(subDirectory, zipFile);
            }

            foreach (string file in files)
            {
                zipFile.EntryFactory.NameTransform = new ZipNameTransform(directoryName);
                zipFile.Add(file);

                EditorUtility.DisplayProgressBar("Compressing", "Asset files are getting compressed", _totalCompressedFileCount / (float) _numberOfFilesToBeCompressed);
                _totalCompressedFileCount++;
            }

            EditorUtility.ClearProgressBar();
        }
    }

    [System.Serializable]
    public class GetPlotsServiceDO
    {
        public int status;
        public List<PlotDO> result;
        public string error;
    }

    [System.Serializable]
    public class PlotDO
    {
        public string plotNumber;
    }
}
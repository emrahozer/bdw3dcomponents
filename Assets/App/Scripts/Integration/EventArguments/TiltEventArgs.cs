﻿using System;

namespace App.Scripts.Integration.EventArguments
{
    public class TiltEventArgs : EventArgs
    {
        public float Delta;
        public TiltEventArgs(float delta)
        {
            Delta = delta;
        }

    }
}

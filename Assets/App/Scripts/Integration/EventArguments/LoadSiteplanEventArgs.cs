﻿using System;

namespace App.Scripts.Integration.EventArguments
{
    public class LoadSiteplanEventArgs : EventArgs
    {
        public string Path { get; set; }
        public string SceneName { get; set; }
        public LoadSiteplanEventArgs(string path, string sceneName)
        {
            Path = path;
            SceneName = sceneName;
        }
    }
}

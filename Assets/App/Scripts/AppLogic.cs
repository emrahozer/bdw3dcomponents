﻿using System;
using System.Collections;
using App.Scripts.CameraControllers;
using App.Scripts.Integration.EventArguments;
using App.Scripts.Integration.Utility;
using App.Scripts.Markers;
using App.Scripts.Model;
using UnityEngine;

namespace App.Scripts
{
    public class AppLogic : MonoBehaviour
    {
        private static AppLogic _instance;
        private CameraState _cameraState;
        private bool _isInInteraction;
        private InteractiveRegion[] _regionList;
        private Vector3 _rtsCameraInitialPosition;
        private Quaternion _rtsCameraInitialRotation;

        [HideInInspector]
        public Camera CurrentCamera { get; private set; }

        [HideInInspector]
        public GameComponents Components { get; set; }

        public static AppLogic Instance
        {
            set
            {
                if (_instance == null)
                    _instance = value;
            }
            get { return _instance; }
        }

        public CameraState CameraState
        {
            get { return _cameraState; }
            set
            {
                _cameraState = value;

                Components.RtsCamera.gameObject.SetActive(_cameraState == CameraState.RTS);
                Components.FpsCamera.gameObject.SetActive(_cameraState == CameraState.FPS);

                Components.RtsInteractiveRegions.gameObject.SetActive(_cameraState == CameraState.RTS);
                Components.FpsInteractiveRegions.gameObject.SetActive(_cameraState == CameraState.FPS);

                CurrentCamera = _cameraState == CameraState.RTS ? Components.RtsCamera : Components.FpsCamera;
            }
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            Components.RtsCamera.gameObject.AddComponent<RtsCameraController>();
            Components.FpsCamera.gameObject.AddComponent<FpsCameraController>();
        }

        public void Initialize()
        {

            _isInInteraction = UnityUwpInterop.Instance.IsInInteraction;

            if (Components != null && Components.InteractivityContainer != null)
            {
                CameraState = CameraState.RTS;
                _regionList = Components.InteractivityContainer.GetComponentsInChildren<InteractiveRegion>(true);
                Components.InteractivityContainer.gameObject.SetActive(_isInInteraction);
                _rtsCameraInitialPosition = Components.RtsCamera.transform.position;
                _rtsCameraInitialRotation = Components.RtsCamera.transform.rotation;
               
            }

            UnityUwpInterop.Instance.InteractionStatusChanged += OnInteractionChanged;
            UnityUwpInterop.Instance.TapReceived += OnTapReceived;
            UnityUwpInterop.Instance.StatusUpdated += OnStatusUpdated;
            UnityUwpInterop.Instance.FilterUpdated += OnFilterUpdated;
            UnityUwpInterop.Instance.StreetModeUpdated += OnStreetModeUpdated;
            UnityUwpInterop.Instance.AppReset += OnAppReset;

            AssignMaterials();

            UnityUwpInterop.Instance.RaiseSceneLoaded();
        }

        private void OnAppReset(object sender, EventArgs e)
        {
            UnityMainThreadDispatcher.Enqueue(ResetCameras());
        }

        private IEnumerator ResetCameras()
        {
            CameraState = CameraState.RTS;
            Components.RtsCamera.GetComponent<RtsCameraController>().Reset();
            Components.RtsCamera.transform.rotation = _rtsCameraInitialRotation;
            Components.RtsCamera.transform.position = _rtsCameraInitialPosition;
            yield return null;
        }

        private void AssignMaterials()
        {
            var meshList = Components.InteractivityContainer.GetComponentsInChildren<MeshInteractiveRegion>(true);

            foreach (var meshInteractiveRegion in meshList)
            {
                meshInteractiveRegion.InternalAvailableMaterial = Components.AvailableMeshMaterial;
                meshInteractiveRegion.InternalReservedMaterial = Components.ReservedMeshMaterial;
                meshInteractiveRegion.InternalSoldMaterial = Components.SoldMeshMaterial;
                meshInteractiveRegion.UpdateMaterialsAndVisibility();
            }
        }

        private void OnDisable()
        {
            UnityUwpInterop.Instance.InteractionStatusChanged -= OnInteractionChanged;
            UnityUwpInterop.Instance.TapReceived -= OnTapReceived;
            UnityUwpInterop.Instance.StatusUpdated -= OnStatusUpdated;
            UnityUwpInterop.Instance.FilterUpdated -= OnFilterUpdated;
            UnityUwpInterop.Instance.StreetModeUpdated -= OnStreetModeUpdated;
            UnityUwpInterop.Instance.AppReset -= OnAppReset;
        }

        private void OnFilterUpdated(object sender, EventArgs e)
        {
            UnityMainThreadDispatcher.Enqueue(UpdateFilter());
        }

        private void OnStatusUpdated(object sender, EventArgs e)
        {
            UnityMainThreadDispatcher.Enqueue(UpdateStatus());
        }

        private void OnTapReceived(object sender, TapEventArguments e)
        {
            UnityMainThreadDispatcher.Enqueue(OnShortTap(e.Position));
        }

        private void OnInteractionChanged(object sender, EventArgs e)
        {
            _isInInteraction = UnityUwpInterop.Instance.IsInInteraction;
            UnityMainThreadDispatcher.Enqueue(SwitchVisibility());
        }

        private IEnumerator UpdateStatus()
        {
            foreach (var interactiveRegion in _regionList)
            {
                int value;
                if (UnityUwpInterop.Instance.StatusDictionary.TryGetValue(interactiveRegion.PlotId.ToString(), out value))
                    interactiveRegion.Status = (PlotStatus) value;
            }
            yield return null;
        }

        private IEnumerator UpdateFilter()
        {
            foreach (var interactiveRegion in _regionList)
            {
                bool value;
                if (UnityUwpInterop.Instance.IsInFilterDictionary.TryGetValue(interactiveRegion.PlotId.ToString(), out value))
                    interactiveRegion.IsInFilter = value;
            }
            yield return null;
        }

        private IEnumerator SwitchVisibility()
        {
            Debug.Log("On Interaction Changed");
            Debug.Log("Components " + Components);
            Debug.Log("Interactivity Container" + Components.InteractivityContainer);
            if (Components.ModelContainer != null && Components.InteractivityContainer != null)
            {
                Components.InteractivityContainer.gameObject.SetActive(_isInInteraction);
            }
            yield return null;
        }

        private IEnumerator OnShortTap(Vector2 pos)
        {
            var dpiPos = new Vector2(pos.x * Screen.dpi/96.0f, pos.y * Screen.dpi / 96.0f);

            RaycastHit hit;

            var ray = CurrentCamera.ScreenPointToRay(dpiPos);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                var region = hit.collider.transform.GetComponent<InteractiveRegion>();
                if (region != null)
                {
                    Debug.Log("Dispatch " + region.PlotId);
                    UnityUwpInterop.Instance.DispatchOpenPlotEvent(region.PlotId.ToString());
                }
                yield return null;

                var vrSpot = hit.collider.transform.GetComponent<VrHotSpot>();
                if (vrSpot != null)
                    EnterFpsState(vrSpot.transform.position);
            }
        }

        private void EnterFpsState(Vector3 position)
        {
            var markerPosition = position;
            markerPosition.y = Components.FpsCamera.transform.position.y;

            if (CameraState == CameraState.RTS)
            {
                CameraState = CameraState.FPS;
                Components.FpsCamera.transform.position = markerPosition;
                Components.FpsCamera.transform.rotation = Quaternion.identity;
                UnityUwpInterop.Instance.IsInStreetMode = true;
                StartCoroutine(UpdateStatus());
            }
            else
            {
                Components.FpsCamera.GetComponent<FpsCameraController>().DesiredPosition = markerPosition;
            }
        }

        private void OnStreetModeUpdated(object sender, CameraModeEventArguments cameraModeEventArguments)
        {
            if (!cameraModeEventArguments.IsInFpsMode)
                UnityMainThreadDispatcher.Enqueue(ExitFpsState());
        }

        public IEnumerator ExitFpsState()
        {
            CameraState = CameraState.RTS;
            Components.FpsCamera.GetComponent<FpsCameraController>().DesiredPosition = Vector3.zero;
            yield return StartCoroutine(UpdateStatus());
        }
    }
}